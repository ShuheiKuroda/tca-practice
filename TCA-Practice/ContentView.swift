//
//  ContentView.swift
//  TCA-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import SwiftUI
import ComposableArchitecture

struct ContentView: View {
  var body: some View {
    NavigationView {
      List {
        NavigationLink("TCA transition") {
          ContentsListView(
            store: Store(
              initialState: ContentsListState(
                selectedCount: 0
              ),
              reducer: contentsListReducer,
              environment: ContentsListEnvironment(
                contentClient: .live,
                mainQueue: .main,
                uuid: UUID.init
              )
            )
          )
        }
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

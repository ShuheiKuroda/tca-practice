//
//  ContentsDetailCore.swift
//  TCA-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import Foundation
import ComposableArchitecture

struct DetailState: Equatable {
  var contents: ContentState
}

enum DetailAction {
  case nameTextFieldChanged(String)
  case memoTextFieldChanged(String)
  case selectFrequencyButtonTapped
  case deleteButtonTapped
  
}

struct DetailEnvironment {
}

let detailReducer = Reducer<
  DetailState, DetailAction, DetailEnvironment>
{ state, action, envrionment in
  switch action {
  case .nameTextFieldChanged(let name):
    return .none
  case .memoTextFieldChanged(let memo):
    return .none
  case .selectFrequencyButtonTapped:
    return .none
  case .deleteButtonTapped:
    // レコード削除
    // dismiss()
    // リストの再描画はどうなる？ -> ListのonAppearでデータ取得するようにする
    return .none
  }
}

//
//  ContentsDetailView.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/28.
//

import SwiftUI
import ComposableArchitecture

struct ContentsDetailView: View {
  let store: Store<DetailState, DetailAction>
  
  var body: some View {
    WithViewStore(self.store) { viewStore in
      VStack {
        Spacer()
        TextField(
          "名前",
          text: viewStore.binding(get: \.contents.content.name, send: DetailAction.nameTextFieldChanged)
        )
          .frame(height: 50)
          .background(Color.white)
        
        TextField(
          "メモ",
          text: viewStore.binding(get: \.contents.content.memo, send: DetailAction.memoTextFieldChanged)
        )
          .frame(height: 200)
          .background(Color.white)
        
        Spacer()
        
        Button("Clear Completed") {
          viewStore.send(.selectFrequencyButtonTapped, animation: .default)
        }
        
        Spacer()
        
        Button("Clear Completed") {
          viewStore.send(.deleteButtonTapped, animation: .default)
        }
        
        Spacer()
      }
      .background(Color.brown)
      .navigationTitle("詳細")
    }
    
  }
}

struct ContentsDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ContentsDetailView(
          store: Store(
            initialState: DetailState(
              contents: ContentState(id: UUID(), content: Content(id: 0, name: "name", memo: "memo"))
            ),
            reducer: detailReducer,
            environment: DetailEnvironment()
          )
        )
    }
}

//
//  ContentsListView.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/25.
//

import SwiftUI
import ComposableArchitecture
import IdentifiedCollections

struct ContentsListView: View {
  var store: Store<ContentsListState, ContentsListAction>
  
  var body: some View {
    WithViewStore(self.store) { viewStore in
      ZStack(alignment: .bottomTrailing) {
        List {
          ForEachStore(
            self.store.scope(state: \.contents, action: ContentsListAction.check(id:action:)),
            content: ContentsRowView.init(store:)
          )
        }
        .navigationTitle("Todos")
        .toolbar {
          ToolbarItem(placement: .navigationBarTrailing) {
            Button("Add") {
//              viewStore.send(.addButtonTapped)
            }
          }
        }
        
        HStack {
          // 吹き出し
          if viewStore.isShowSpeechBubble {
            Text(#"準備が完了したら”タップ"してね"#)
              .frame(height: 55)
              .padding(.horizontal, 15)
              .background(Color.yellow)
              .padding(.bottom, 16)
              .onTapGesture {
                viewStore.send(.speechBubbleTapped)
              }
          }
          
          Button(action: {
            // チェックを消す
          }) {
            ZStack(alignment: .topLeading) {
              Group {
                if viewStore.selectedCount > 0 {
                  Image(systemName: "house")
                } else {
                  Text("+")
                }
              }
                .foregroundColor(.black)
                .font(.system(size: 20))
                .frame(width: 55, height: 55)
                .background(Color.yellow)
                .clipShape(Circle())
                .shadow(color: .gray, radius: 3, x: 0, y: 3)
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 16.0, trailing: 16.0))
              
              if viewStore.selectedCount > 0 {
                Text("残\(viewStore.contents.count - viewStore.selectedCount)")
                  .foregroundColor(.black)
                  .font(.system(size: 11))
                  .frame(width: 32, height: 32)
                  .background(Color.green)
                  .clipShape(Circle())
              }
            }
          }
        }
      }
      .onAppear {
        viewStore.send(.onAppear)
      }
    }
  }
}

struct ContentsListView_Previews: PreviewProvider {
  static var previews: some View {
    ContentsListView(
      store: Store(
        initialState: ContentsListState(),
        reducer: contentsListReducer,
        environment: ContentsListEnvironment(
          contentClient: .mockPreview(),
          mainQueue: .main,
          uuid: UUID.init
        )
      )
    )
  }
}

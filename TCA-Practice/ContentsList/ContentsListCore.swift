//
//  ContentsListCore.swift
//  TCA-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import Foundation
import ComposableArchitecture

struct ContentsListState: Equatable {
  var selectedCount: Int = 0
  var contents = IdentifiedArrayOf<ContentState>()
  var isShowSpeechBubble = false
  var isResetState = true
}

enum ContentsListAction {
  case onAppear
  case tapCheckCompletionButton
  case speechBubbleTapped
  case setSpeechBubble
  case check(id: ContentState.ID, action: ContentAction)
  case fetchContentResponse(Result<[Content], ProviderError>)
}

struct ContentsListEnvironment {
  var contentClient: ContentClient
  var mainQueue: AnySchedulerOf<DispatchQueue>
  var uuid: () -> UUID
}

let contentsListReducer = Reducer
<ContentsListState, ContentsListAction, ContentsListEnvironment>.combine(
  contentReducer.forEach(
    state: \.contents,
    action: /ContentsListAction.check(id:action:),
    environment: { _ in ContentEnvironment() }
  ),
  Reducer { state, action, environment in
    switch action {
    case .onAppear:
      return environment.contentClient.fetch()
        .receive(on: environment.mainQueue)
        .catchToEffect(ContentsListAction.fetchContentResponse)
      
    case .tapCheckCompletionButton:
      return .none
      
    case .speechBubbleTapped:
      state.isShowSpeechBubble.toggle()
      return .none
      
    case .setSpeechBubble:
      if state.selectedCount == 0 {
        state.isShowSpeechBubble = false
        state.isResetState = true
      } else if state.selectedCount > 0 && state.isResetState {
        state.isShowSpeechBubble = true
        state.isResetState = false
      }
      return .none
      
    case .check(id: let id, action: let action):
      state.selectedCount = state.contents.filter { $0.isComplete }.count
      return Effect(value: .setSpeechBubble)
      
    case .fetchContentResponse(.success(let response)):
      let contents = IdentifiedArrayOf<ContentState>(
        uniqueElements: response.map {
          ContentState(
            id: environment.uuid(),
            content: $0
          )
        }
      )
      state.contents = contents
      return .none
    case .fetchContentResponse(.failure):
      return .none
    }
    
  }
)

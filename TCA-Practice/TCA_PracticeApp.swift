//
//  TCA_PracticeApp.swift
//  TCA-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import SwiftUI

@main
struct TCA_PracticeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

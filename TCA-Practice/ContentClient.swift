//
//  ContentClient.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/03/02.
//

import Combine
import ComposableArchitecture

struct ContentClient {
  var fetch: () -> Effect<[Content], ProviderError>
}

extension ContentClient {
  static let live = ContentClient(
    fetch: {
      Provider.shared
        .getContent()
        .eraseToEffect()
    }
  )
}

// MARK: - Mock

extension ContentClient {
  static func mock(
    fetch: @escaping () -> Effect<[Content], ProviderError> = {
      fatalError("Unmocked")
    }
  ) -> Self {
    Self(
      fetch: fetch
    )
  }

  static func mockPreview(
    fetch: @escaping () -> Effect<[Content], ProviderError> = {
      .init(value: [Content(id: 0, name: "namename", memo: "memomemo")])
    }
  ) -> Self {
    Self(
      fetch: fetch
    )
  }
}

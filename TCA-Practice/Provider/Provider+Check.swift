//
//  Provider+Check.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/03/02.
//

import Foundation
import Combine
// for delete: https://designer.mocky.io/manage/delete/b553ea79-eb7e-4ad0-a666-426762f11a01/85P6adsd8Z9s3wTAWaKlZh2dmJOukfTSokHL
// https://run.mocky.io/v3/b553ea79-eb7e-4ad0-a666-426762f11a01

// for delete: https://designer.mocky.io/manage/delete/ef0e7e35-1614-48a7-8fed-ab0c4b7c5d0d/1e6lMEp62LWdYNpGiFmMliMRNfb465eKXLnJ
// https://run.mocky.io/v3/ef0e7e35-1614-48a7-8fed-ab0c4b7c5d0d isCompleteあり

extension Provider {
  func getContent() -> AnyPublisher<[Content], ProviderError> {
    var request = URLRequest(url: URL(string: "https://run.mocky.io/v3/b553ea79-eb7e-4ad0-a666-426762f11a01")!)
       request.httpMethod = "GET"

       return requestPublisher(request)
  }
}


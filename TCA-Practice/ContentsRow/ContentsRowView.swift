//
//  ContentsRowView.swift
//  SwiftUI-Combine-TCA
//
//  Created by shuhei.kuroda on 2022/02/25.
//

import SwiftUI
import ComposableArchitecture

struct ContentsRowView: View {
  var store: Store<ContentState, ContentAction>
  
    var body: some View {
      WithViewStore(self.store) { viewStore in
        ZStack(alignment: .leading) {
          HStack {
            Button(action: { viewStore.send(.checkBoxToggled) }) {
              Image(systemName: viewStore.isComplete ? "checkmark.square" : "square")
            }
            .buttonStyle(.plain)
            
            VStack(alignment: .leading) {
              Text("title: \(viewStore.content.name)")
              Text("memo: \(viewStore.content.memo)")
            }
          }
          
          NavigationLink(
            destination:
              ContentsDetailView(
                store: Store(
                  initialState: DetailState(contents: viewStore.state),
                  reducer: detailReducer,
                  environment: DetailEnvironment()
                )
              )
          ) {
            EmptyView()
          }.buttonStyle(.plain)
          
        }
      }
      
    }
}

struct ContentsRowView_Previews: PreviewProvider {
    static var previews: some View {
        ContentsRowView(
          store: Store(
            initialState: ContentState(
              id: UUID(), content: Content(id: 0, name: "name", memo: "memo")
            ),
            reducer: contentReducer,
            environment: ContentEnvironment(
            )
          )
        )
    }
}

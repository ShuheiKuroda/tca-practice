//
//  ContentsRowCore.swift
//  TCA-Practice
//
//  Created by shuhei.kuroda on 2022/03/03.
//

import Foundation
import ComposableArchitecture

struct ContentState: Equatable, Identifiable {
  let id: UUID
  var content: Content
  var isComplete: Bool = false
}

struct Content: Codable, Equatable, Identifiable {
  var id: Int
  var name: String
  var memo: String
}

enum ContentAction {
  case checkBoxToggled
  case textFieldChanged(String)
}

struct ContentEnvironment {
  
}

let contentReducer = Reducer<ContentState, ContentAction, ContentEnvironment> { state, action, environment in
  switch action {
  case .checkBoxToggled:
    state.isComplete.toggle()
    return .none
  case .textFieldChanged(let text):
    return .none
  }
}

